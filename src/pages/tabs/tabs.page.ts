import { Component } from "@angular/core";

import { TasksPage } from "../tasks/tasks.page";
import { RemindersPage } from "../reminder/reminders.page";

@Component({
    templateUrl: "tabs.page.html",
})
export class TabsPage {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    public TasksPage: any = TasksPage;
    public RemindersPage: any = RemindersPage;

    constructor() { //
    }
}
