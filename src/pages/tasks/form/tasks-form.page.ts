import { Component } from "@angular/core";
import { Task } from "../../../app/models/Task";
import { TasksService } from "../../../app/services/domain/tasks.service";
import { NavController } from "ionic-angular";
import {MomentService} from "../../../app/services/app/moment.service";

@Component({
    selector: "ib-page-simple-form",
    templateUrl: "./tasks-form.page.html"
})
export class TasksFormPage {
    public task: Task;
    public date: any;

    constructor(private taskService: TasksService,
                private navController: NavController,
                private momentService: MomentService) {
        this.task = new Task();
        this.date = momentService.now("YYYY-MM-DD");
    }

    public async save() {
        await this.taskService.save(new Task({
            ...this.task,
            date: this.date
        }));

        await this.navController.goToRoot({ animate: true });
    }
}
