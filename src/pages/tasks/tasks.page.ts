import {Component, OnInit} from "@angular/core";
import {FabContainer, ItemSliding, NavController} from "ionic-angular";
import {TasksFormPage} from "./form/tasks-form.page";
import {GetThingsDone} from "../../app/models/GetThingsDone";
import {Task} from "../../app/models/Task";
import {LoadingService} from "../../app/services/app/loading.service";
import { TasksService } from "../../app/services/domain/tasks.service";

@Component({
    selector: "ib-page-tasks",
    templateUrl: "tasks.page.html",
})
export class TasksPage implements OnInit {
    public projects: GetThingsDone[];

    constructor(private tasksService: TasksService,
                private navCtrl: NavController,
                private loadingService: LoadingService) { }

    public async ngOnInit() {
        this.projects = await this.tasksService.list();
    }
// [SO] Descrição dos principais serviços
    public async toggleDoneStatus(task: Task, slidingItem: ItemSliding) {
        this.loadingService.present();

        slidingItem.close();
        task = await this.tasksService.toggleDoneStatus(task);

        const projectIndex = this.projects.findIndex((_) => _.date === task.date);
        const taskIndex = this.projects[projectIndex].tasks.findIndex((_) => _._id === task._id);
        this.projects[projectIndex].tasks[taskIndex] = task;

        this.loadingService.dismiss();
    }

    public async goToCreate(fabContainer: FabContainer) {
        fabContainer.close();
        await this.navCtrl.push(TasksFormPage);
    }

    public async delete(task: Task) {
        await this.tasksService.delete(task);

        const day = this.projects.filter((_) => _.date === task.date)[0];
        const index = day.tasks.indexOf(task);
        day.tasks.splice(index, 1);
    }
}
