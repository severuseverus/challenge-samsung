import { Component } from "@angular/core";

import { NavController } from "ionic-angular";

@Component({
  selector: "ib-page-reminders",
  templateUrl: "reminders.page.html",
})
export class RemindersPage {

  constructor(public navCtrl: NavController) {

  }

}
