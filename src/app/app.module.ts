import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { HttpClientModule } from "@angular/common/http";

import { MyApp } from "./app.component";
import { RemindersPage } from "../pages/reminder/reminders.page";
import { TasksPage } from "../pages/tasks/tasks.page";
import { TabsPage } from "../pages/tabs/tabs.page";
import { TasksFormPage } from "../pages/tasks/form/tasks-form.page";
import { TasksService } from "./services/domain/tasks.service";
import { ToastService } from "./services/app/toast.service";
import { MomentService } from "./services/app/moment.service";
import { HttpService } from "./services/app/http.service";
import { LoadingService } from "./services/app/loading.service";

@NgModule({
    declarations: [
        MyApp,
        RemindersPage,
        TasksPage,
        TabsPage,
        TasksFormPage
    ],
    imports: [
        BrowserModule,
        TasksService,
        ToastService,
        MomentService,
        HttpClientModule,
        HttpService,
        LoadingService,
        IonicModule.forRoot(MyApp, {
            menuType: "push",
            platforms: {
                ios: {
                    menuType: "overlay",
                }
            }
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        RemindersPage,
        TasksPage,
        TabsPage,
        TasksFormPage
    ],
    providers: [SplashScreen]
})
export class AppModule { }
