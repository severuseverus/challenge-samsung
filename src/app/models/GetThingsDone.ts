import {BaseModel} from "./BaseModel";
import {Task} from "./Task";
import {MomentService} from "../services/app/moment.service";

export class GetThingsDone extends BaseModel {
    public date: string;
    public tasks: Task[];

    constructor(model?: any) {
        super(null);

        const momentService = new MomentService();

        this.date = momentService.format(model.date);
        this.tasks = model.tasks.map((task: any) => new Task(task));
    }
}
