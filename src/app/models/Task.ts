import {BaseModel} from "./BaseModel";
import {MomentService} from "../services/app/moment.service";

export class Task extends BaseModel {
    public _id: number;
    public label: string;
    public done: boolean;
    public date: string;

    constructor(model?: any) {
        const momentService = new MomentService();

        super(model != null ? { ...model, date: momentService.format(model.date) } : {
            label: "",
            done: false,
            date: momentService.now()
        });
    }
}
