import * as _ from "lodash";
export class BaseModel {
    [key: string]: any;

    constructor(data?: object) {
        this.populate(data);
    }

    protected populate(data: object) {
        _.forOwn(data, (value, key) => {
            this[key] = value;
        });
    }
}
