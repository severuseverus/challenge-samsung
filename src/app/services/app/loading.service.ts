import {NgModule} from "@angular/core";
import {Loading, LoadingController} from "ionic-angular";

@NgModule()
export class LoadingService {
    private loading: Loading;
    private active: boolean;

    constructor(private loadingController: LoadingController) {
        this.reset();
    }

    public present() {
        if (!this.active) {
            this.active = true;
            this.loading.present();
        }
    }

    public dismiss() {
        this.loading.dismiss();
        this.reset();
    }

    private reset() {
        this.loading = this.loadingController.create({
            content: "Please wait..."
        });
        this.active = false;
    }
}
