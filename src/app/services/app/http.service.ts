import {NgModule} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@NgModule()
export class HttpService {
    constructor(private httpClient: HttpClient) { }

    public async get(endpoint: string): Promise<any> {
        return new Promise((res) => {
            this.httpClient.get(`http://127.0.0.1:3000${endpoint}`)
                .subscribe((data: any) => {
                    res(data._data);
                });
        });
    }

    public async post(endpoint: string, body: object): Promise<any> {
        return new Promise((res) => {
            this.httpClient.post(`http://127.0.0.1:3000${endpoint}`, body)
                .subscribe((data: any) => {
                    res(data._data);
                });
        });
    }

    public async delete(endpoint: string): Promise<any> {
        return new Promise((res) => {
            this.httpClient.delete(`http://127.0.0.1:3000${endpoint}`)
                .subscribe((data: any) => {
                    res(data._data);
                });
        });
    }
}
