import {ToastController} from "ionic-angular";
import {NgModule} from "@angular/core";

@NgModule()
export class ToastService {
    constructor(private toastCtrl: ToastController) { }

    public showToastMessage(message: string) {
        const toast = this.toastCtrl.create({
            message,
            duration: 2000,
            position: "bottom"
        });

        toast.present();
    }

}
