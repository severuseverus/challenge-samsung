import * as moment from "moment";
import {NgModule} from "@angular/core";

@NgModule()
export class MomentService {

    public now(format?: string): string {
        return moment().format(format != null ? format : "DD/MM/YYYY").toString();
    }

    public  format(date: string, currentFormat?: string, newFormat?: string) {
        if (currentFormat != null) {
            return moment(date, currentFormat).format(newFormat != null ? newFormat : "DD/MM/YYYY");
        }

        return moment(date).format(newFormat != null ? newFormat : "DD/MM/YYYY");
    }
}
