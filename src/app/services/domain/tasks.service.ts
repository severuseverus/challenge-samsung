import {NgModule} from "@angular/core";
import {Task} from "../../models/Task";
import {ToastService} from "../app/toast.service";
import {GetThingsDone} from "../../models/GetThingsDone";
import {MomentService} from "../app/moment.service";
import {HttpService} from "../app/http.service";

@NgModule()
export class TasksService {
    constructor(private toastService: ToastService,
                private momentService: MomentService,
                private httpService: HttpService) { }

    public async list(): Promise<GetThingsDone[]> {
        const tasksByDay = await this.httpService.get("/tasks");
        return tasksByDay.map((tasksInDay: any) => new GetThingsDone(tasksInDay));
    }

    public async save(model: Task) {
        const task = {
            ...model,
            date: this.momentService.format(model.date, "DD/MM/YYYY", "YYYY-MM-DD ")
        };

        await this.httpService.post("/tasks", task);
        this.toastService.showToastMessage("Nova tarefa cadastrada! JUST DO IT.");
    }

    public async delete(task: Task) {
        await this.httpService.delete(`/tasks/${task._id}`);
    }

    public async toggleDoneStatus(model: Task): Promise<Task> {
        model.done = !model.done;
        const task = {
            ...model,
            date: this.momentService.format(model.date, "DD/MM/YYYY", "YYYY-MM-DD ")
        };

        await this.httpService.post("/tasks", task);

        const message = task.done ? "Parabéns! Mais um tarefa feita!" : "Vamos fingir que nada aconteceu.";
        this.toastService.showToastMessage(message);

        return model;
    }
}
